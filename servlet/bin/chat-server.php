<?php
require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once dirname(__DIR__) . '/src/MyApp/Command.php';

$host = gethostname();
$ip = gethostbyname($host);
$port = 8080;

$chatComponent = new MyApp\Command();
$address = '0.0.0.0';
$ws = new Ratchet\App($ip, $port, $address);
$server = new Ratchet\WebSocket\WsServer($chatComponent);
$ws->route('/command', $server, array('*'));
$ws->route('/command', $server, array('*'), gethostname());
$ws->route('/command', $server, array('*'), 'localhost');
$ws->route('/command', $server, array('*'), '127.0.0.1');
$ws->run();

?>
