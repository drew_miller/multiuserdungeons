<?php

function makeValuesReferenced($arr){
    $refs = array();
    foreach($arr as $key => $value) {
        $refs[$key] = &$arr[$key];
    }
    return $refs;

}

// Replace PHP's stupid $stmt->bind_value() function.
// 
// If array values are used in a $stmt->bind_param() function, then the array 
// values become references in that array. This causes bad and unexpected 
// behavior later in code when values are assigned to the array with references 
// and those values are modified, changing each other and the original array.
function bindParams() {
    $copies = array();
    $stmt = func_get_arg(0);
    $params = array_slice(func_get_args(), 1);
    for($i = 0 ; $i < func_num_args(); $i++) {
        call_user_func_array(array($stmt, 'bind_param'), makeValuesReferenced($params));
    }
}

function deleteValue(&$array, $value) {
    if(($key = array_search($value, $array)) !== false) {
        unset($array[$key]);
    }
}

?>
