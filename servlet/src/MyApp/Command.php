<?php
namespace MyApp;
use Guzzle\Http\Message\RequestInterface;
use Guzzle\Http\Message\Request;
use Ratchet\ConnectionInterface;
use Ratchet\Http\HttpServerInterface;
use Ratchet\MessageComponentInterface;

include_once "Util.php";

// WebSocket message component handler
class Command implements MessageComponentInterface {
    protected $clients;
    protected $mysqli;
    protected $nameToClient;
    
    // Utility functions
    public function startsWith($haystack, $needle) {
         $length = strlen($needle);
         return (substr($haystack, 0, $length) === $needle);
    }

    public function endsWith($haystack, $needle) {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }
        return (substr($haystack, -$length) === $needle);
    }
    
    public function initDb() {
        // Use the UTC timezone.
        date_default_timezone_set('UTC');
        // Clear the users table
        $this->mysqli->query('DROP TABLE users');
        $this->mysqli->query('DROP TABLE locations');
        // Create the locations table
        if ($this->mysqli->query('SHOW TABLES LIKE "locations"')->num_rows < 1) {
            $this->mysqli->query('
            CREATE TABLE IF NOT EXISTS locations (
                x INT NOT NULL DEFAULT 2,
                y INT NOT NULL DEFAULT 2,
                z INT NOT NULL DEFAULT 0,
                isNavigable INT NOT NULL DEFAULT 0,
                name VARCHAR(255),
                PRIMARY KEY (x, y, z)
            )');
            // Fill in the world
            for ($z=0; $z<2; $z++) {
                for ($y=0; $y<4; $y++) {
                    for ($x=0; $x<4; $x++) {
                        $stmt = $this->mysqli->query("INSERT INTO locations(x, y, z) VALUES($x, $y, $z)");
                    }
                }
            }
            // Special locations
            $this->mysqli->query("UPDATE locations SET isNavigable=1, name='Illyria' 
            WHERE x=0 AND y=0 AND z=0");
            $this->mysqli->query("UPDATE locations SET isNavigable=1, name='Macedon' 
            WHERE x=1 AND y=0 AND z=0");
            $this->mysqli->query("UPDATE locations SET isNavigable=1, name='Chalcidice' 
            WHERE x=2 AND y=0 AND z=0");
            $this->mysqli->query("UPDATE locations SET isNavigable=1, name='the Hellespont' 
            WHERE x=3 AND y=0 AND z=0");
            
            $this->mysqli->query("UPDATE locations SET isNavigable=1, name='Epirus' 
            WHERE x=0 AND y=1 AND z=0");
            $this->mysqli->query("UPDATE locations SET isNavigable=1, name='Olympus' 
            WHERE x=1 AND y=1 AND z=0");
            $this->mysqli->query("UPDATE locations SET isNavigable=1, name='Euboea' 
            WHERE x=2 AND y=1 AND z=0");
            $this->mysqli->query("UPDATE locations SET isNavigable=1, name='Troy' 
            WHERE x=3 AND y=1 AND z=0");
            
            $this->mysqli->query("UPDATE locations SET isNavigable=1, name='Sparta' 
            WHERE x=1 AND y=2 AND z=0");
            $this->mysqli->query("UPDATE locations SET isNavigable=1, name='Athens' 
            WHERE x=2 AND y=2 AND z=0");
            $this->mysqli->query("UPDATE locations SET isNavigable=1, name='Ionia' 
            WHERE x=3 AND y=2 AND z=0");

            $this->mysqli->query("UPDATE locations SET isNavigable=1, name='Crete' 
            WHERE x=2 AND y=3 AND z=0");
            $this->mysqli->query("UPDATE locations SET isNavigable=1, name='Rhodes' 
            WHERE x=3 AND y=3 AND z=0");

            $this->mysqli->query("UPDATE locations SET isNavigable=1, name='Olympus summit' 
            WHERE x=1 AND y=1 AND z=1");
        }
        // Create the users table
        $result = $this->mysqli->query("
        CREATE TABLE IF NOT EXISTS users (
            sessionId VARCHAR(255) NOT NULL,
            name VARCHAR(255) NOT NULL,
            lastModified DOUBLE NOT NULL,
            x INT NOT NULL DEFAULT 2,
            y INT NOT NULL DEFAULT 2,
            z INT NOT NULL DEFAULT 0,
            FOREIGN KEY (x, y, z) REFERENCES locations(x, y, z)
        )");
        // TODO Clean up dead users
        // $now = time();
        // $this->mysqli->query("DELETE * FROM users WHERE lastModified < $yearAgo");
        // $yearAgo = $now-3.154E7;
    }

    public function __construct() {
        $this->clients = new \SplObjectStorage();
        $this->mysqli = new \mysqli('127.0.0.1', 'root', '', 'dungeons');
        $this->initDb($this->mysqli);
        echo "ready\n";
    }

    public function lookupNameFromSessionId($sessionId) {
        $stmt = $this->mysqli->prepare('SELECT name FROM users WHERE sessionId=?');
        bindParams($stmt, 's', $sessionId);
        $stmt->execute();
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
        if ($row) {
            return $row['name'];
        } else {
            return NULL;
        }
    }
    
    public function getNamesAtLocation($location) {
        $members = array();
        $stmt = $this->mysqli->prepare('SELECT name FROM users WHERE x=? AND y=? AND z=?');
        bindParams($stmt, 'iii', $location['x'], $location['y'], $location['z']);
        $stmt->execute();
        $result = $stmt->get_result();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            $members[$i] = $row['name'];
            $i++;
        }
        return $members;
    }
    
    public function getLocation($name) {
        $stmt = $this->mysqli->prepare('SELECT x, y, z FROM users WHERE name=?');
        bindParams($stmt, 's', $name);
        $stmt->execute();
        $result = $stmt->get_result();
        $location = $result->fetch_assoc();
        $stmt = $this->mysqli->prepare('SELECT name FROM locations WHERE x=? AND y=? AND z=?');
        bindParams($stmt, 'iii', $location['x'], $location['y'], $location['z']);
        $stmt->execute();
        $result = $stmt->get_result();
        $location['name'] = $result->fetch_assoc()['name'];
        return $location;
    }
    
    public function printHelp($client) {
        $out = array('pre'=>
'help
    Show this helpful information again.
name <name>
    Set or change your name to <name>.
    You need a name before you can do anything else.
look
    See who else is in the same region as you.
pwd
    Tell you where you are.
say <stuff>
    Say <stuff> to everyone in the same region as you.
tell <person> <stuff>
    Say <stuff> to <person>.
yell <stuff>
    Yell <stuff> to everyone in the world.
whoami
    Tell you what your name is.',
                'setTerm'=>'');
        $client->send(json_encode($out));
    }
    
    public function announceNameChange($clientName, $newClientName) {
        $out = array('message'=>"$clientName has changed name to $newClientName.");
        $out_json = json_encode($out);
        foreach ($this->nameToClient as $name=>$client) {
            if ($clientName === $name) {
                continue;
            }
            $client->send($out_json);
        }
    }
    
    public function announceDeparture($clientName) {
        $location = $this->getLocation($clientName);
        $names = $this->getNamesAtLocation($location);
        deleteValue($names, $clientName);
        $out = array('message'=>"$clientName departs from {$location['name']}.");
        $out_json = json_encode($out);
        foreach ($names as $name) {
            $this->nameToClient[$name]->send($out_json);
        }
    }
    
    public function announceArrival($clientName) {
        $location = $this->getLocation($clientName);
        $names = $this->getNamesAtLocation($location);
        deleteValue($names, $clientName);
        $out = array('message'=>"$clientName arrives at {$location['name']}.");
        $out_json = json_encode($out);
        foreach ($names as $name) {
            $this->nameToClient[$name]->send($out_json);
        }
        $out = array('message'=>"You arrive in {$location['name']}.");
        $this->nameToClient[$clientName]->send(json_encode($out));
    }
    
    public function look($clientName) {
        // Figure out who is in the user's dungeon
        $location = $this->getLocation($clientName);
        // Tell the user who else is in the current location
        $names = $this->getNamesAtLocation($location);
        deleteValue($names, $clientName);
        $out = $location;
        $out['message'] = '';
        if (count($names) == 0) {
            $out['message'] .= 'You are alone.';
        } else {
            $out['message'] .= "You see ";
            if (count($names) == 1) {
                $out['message'] .= "{$names[0]}.";
            } else if (count($names) == 2) {
                $out['message'] .= "{$names[0]} and {$names[1]}.";
            } else {
                for ($i=0; $i<count($names)-1; $i++) {
                    // Oxford comma or get out
                    $out['message'] .= $names[$i].", ";
                }
                $out['message'] .= 'and '.$names[count($names)-1].'.';
            }
        }
        $out['setTerm'] = '';
        $this->nameToClient[$clientName]->send(json_encode($out));
    }
    
    public function pwd($name) {
        // Tell the current where he is
        $location = $this->getLocation($name);
        $out = $location;
        $out['setTerm'] = '';
        $out['message'] = "You are in {$location['name']}.";
        $this->nameToClient[$name]->send(json_encode($out));
    }
    
    public function onOpen(ConnectionInterface $conn, RequestInterface $request = null) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);
        echo "New connection! ({$conn->resourceId})\n";
    }
    
    public function onMessage(ConnectionInterface $from, $msg) {
        $sessionId = $from->resourceId;
        $clientName = $this->lookupNameFromSessionId($sessionId);
        if ($msg === 'help') {
            $this->printHelp($from);
        } else if (preg_match('/^name \\w+$/', $msg)) {
            $newClientName = substr($msg, strlen('name '));
            if ($newClientName === $clientName) {
                $out = array('message'=>"$clientName is already your name.");
                $from->send(json_encode($out));
            } else {
                $changeName = false;
                if ($clientName === NULL) {
                    $changeName = true;
                } else {
                    $stmt = $this->mysqli->prepare('SELECT name FROM users WHERE name=?');
                    bindParams($stmt, 's', $newClientName);
                    $stmt->execute();
                    $result = $stmt->get_result();
                    if (!($row = $result->fetch_assoc())) {
                        $changeName = true;
                    }
                }
                if ($changeName) {
                    $stmt = $this->mysqli->prepare('UPDATE users SET name=? WHERE sessionId=?');
                    bindParams($stmt, 'ss', $newClientName, $sessionId);
                    $stmt->execute();
                    $stmt = $this->mysqli->prepare('INSERT IGNORE INTO users(sessionId, name, lastModified) VALUES (?, ?, ?)');
                    bindParams($stmt, 'ssd', $sessionId, $newClientName, time());
                    $stmt->execute();
                    $result = $stmt->get_result();
                    unset($this->nameToClient[$clientName]);
                    $this->nameToClient[$newClientName] = $from;
                    if ($clientName === NULL) {
                        $this->announceArrival($newClientName);
                        $this->pwd($newClientName);
                        $this->look($newClientName);
                    } else {
                        $this->announceNameChange($clientName, $newClientName);
                    }
                    $out = array(
                            'message'=>"Henceforth you shall be known as $newClientName.",
                            'name'=>$newClientName,
                            'setTerm'=>'',
                            'x'=>2,
                            'y'=>2,
                            'z'=>0);
                    $from->send(json_encode($out));
                } else {
                    $out = array('message'=>"The name $newClientName is already taken.");
                    $from->send(json_encode($out));
                }
            }
        } else if ($clientName != NULL) {
            if (("Who am I?" === $msg) || ("whoami" === $msg)) {
                // Tell the current who he is
                $out = array(
                        'message'=>"You are $clientName.",
                        'setTerm'=>'');
                $from->send(json_encode($out));
            } else if (("Where am I?" === $msg) || ("pwd" === $msg)) {
                $this->pwd();
            } else if(('look' === $msg ) || ('ls' === $msg)) {
                $out = $this->look($clientName);
                $from->send(json_encode($out));
            } else if (preg_match('/^yell .+$/', $msg)) {
                // Yell to everyone but the current user
                $msg = substr($msg, strlen('yell '));
                $out = array('message'=>"$clientName yells: $msg");
                foreach ($this->nameToClient as $recipient=>$recipientClient) {
                    if ($recipient === $clientName) {
                        continue;
                    }
                    $recipientClient->send(json_encode($out));
                }
                // Tell the current user that he yelled
                $out = array(
                        'setTerm'=>'',
                        'message'=>"You yell: $msg");
                $from->send(json_encode($out));
            } else if (preg_match('/^say .+$/', $msg)) {
                // Get the client's position
                $msg = substr($msg, strlen('say '));
                $stmt = $this->mysqli->prepare('SELECT x, y, z FROM users WHERE name=?');
                bindParams($stmt, 's', $clientName);
                $stmt->execute();
                $result = $stmt->get_result();
                $location = $result->fetch_assoc();
                // Say the message to other users
                $namesAtLocation = $this->getNamesAtLocation($location);
                foreach ($namesAtLocation as $name) {
                    if ($name === $clientName) {
                        continue;
                    }
                    $out = array('message'=>"$clientName says: $msg");
                    $this->nameToClient[$name]->send(json_encode($out));
                }
                // Say to the current user
                $out = array(
                        'setTerm'=>'',
                        'message'=>"You say: $msg");
                $from->send(json_encode($out));
            } else if (preg_match('/^tell \\w+ .+$/', $msg)) {
                // Tell a recipient something
                $msg = substr($msg, strlen('tell '));
                $exploded = explode(' ', $msg, 2);
                $recipient = $exploded[0];
                $msg = $exploded[1];
                if ($recipient === $clientName) {
                    $returnValue = array('message'=>"You to yourself: $msg");
                    $from->send(enocde_json($returnValue));
                } else if (in_array($recipient, $this->nameToClient)) {
                    $unencoded = array('message'=>"$clientName: $msg");
                    $this->nameToClient[$recipient]->send(json_encode($unencoded));
                    $unencoded = array(
                            'setTerm'=>'',
                            'message'=>"You to $clientName: $msg");
                    $from->send(json_encode($unencoded));
                } else {
                    $unencoded = array(
                            'setTerm'=>'',
                            'message'=>"$recipient is not here.");
                    $from->send(json_encode($unencoded));
                }
            } else if ($msg === 'north' || $msg === 'south'
                    || $msg === 'east' || $msg === 'west'
                    || $msg==='up' || $msg==='down') {
                $this->announceDeparture($clientName);
                $startLocation = $this->getLocation($clientName);
                $endLocation = $startLocation;
                if ($msg === 'east') {
                    $endLocation['x'] = ($endLocation['x']+1)%4;
                } else if ($msg === 'west') {
                    $endLocation['x'] = ($endLocation['x']-1+4)%4;
                } else if ($msg === 'north') {
                    $endLocation['y']--;
                } else if ($msg === 'south') {
                    $endLocation['y']++;
                } else if ($msg === 'up') {
                    $endLocation['z']++;
                } else if ($msg === 'down') {
                    $endLocation['z']--;
                }
                // Determine whether the desired future position can be reached
                $stmt = $this->mysqli->prepare('SELECT x, y, z, isNavigable, name FROM locations WHERE x=? AND y=? AND z=?');
                bindParams($stmt, 'iii', $endLocation['x'], $endLocation['y'], $endLocation['z']);
                $stmt->execute();
                $result = $stmt->get_result();
                $endLocation = $result->fetch_assoc();
                if (($endLocation!=NULL) && ($endLocation['isNavigable'])) {
                    // Future location is reachable. Move to the new location.
                    $stmt = $this->mysqli->prepare('UPDATE users SET x=?, y=?, z=? WHERE sessionId=?');
                    bindParams($stmt, 'iiis', $endLocation['x'], $endLocation['y'], $endLocation['z'], $sessionId);
                    $stmt->execute();
                    $this->announceArrival($clientName);
                    $this->look($clientName);
                } else {
                    // Can't move to the new location
                    $errorMessages = array(
                            'Who do you think you are? You can\'t go there!',
                            'Nope.',
                            'That is not possible.',
                            'Impossible.');
                    $out = array(
                            'setTerm'=>'',
                            'message'=>$errorMessages[array_rand($errorMessages)]);
                    $from->send(json_encode($out));
                }
            } else {
                $this->printHelp($from);
                $out = array('message'=>'Invalid command');
                $from->send(json_encode($out));
            }
        } else {
            $out = array(
                    'setTerm'=>'name <your_name>',
                    'highlightStart'=>strlen('name '),
                    'highlightEnd'=>strlen('name <your_name>'),
                    'message'=>'Who are you?');
            $from->send(json_encode($out));
        }
    }
    
    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $sessionId = $conn->resourceId;
        $clientName = $this->lookupNameFromSessionId($sessionId);
        $this->announceDeparture($clientName);
        unset($this->nameToClient[$clientName]);
        $stmt = $this->mysqli->prepare('DELETE FROM users WHERE sessionId=?');
        bindParams($stmt, 's', $sessionId);
        $stmt->execute();
        $this->clients->detach($conn);
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }

}
?>
